public class aula29 {  // laço de repetição FOR I

    public static void main(String[] args) {

        for (int i = 0; i <= 10; ++i){  // instrução for, variavel inteira nome i de valor 10,  incrementa uma unidade
           // enquanto i menor ou igual a 10

            System.out.println( i );  // imprimir i


            /*
            for(part1,part2,part3;)
            * part1: declaramos uma varíavel
            * part2: colocamos a condição para que o looping continue a executar ou seja terminado
            * part3: incrementamos a varíavel
            */
        }

    }
}
